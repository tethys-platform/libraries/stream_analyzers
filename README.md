Stream Analyzers
================

Video and KLV MPEG-TS Stream analyzers.

        const TEST_DATA: [u8; xxx] = ...
        let mut analyzer = video_stream_analyzer::VideoStreamAnalyzer::new(false, false);
        let mut res = vec![];

        for chunk in TEST_DATA.chunks_exact(188) {
            match analyzer.push_ts_packet(&chunk) {
                Some(data) => res.push(data), // emits data on each video frame boundary
                _ => (),
            }
        }



