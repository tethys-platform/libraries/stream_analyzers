use parsers::{
    av1,
    h264,
    h265,
    mpegts,
    video_nal_parser::{VideoNalParser, NalAnalysisResult},
};
use log::{trace, warn};
use std::collections::{BTreeSet, HashMap};

// A chunk of TS data delimited mostly by boundaries of a video frame.
// May contain non-video data.
#[derive(Clone, Debug)]
pub struct TsChunkVideoFrame {
    pub is_iframe: bool, // This chunk contains an I-frame
    pub is_nonref_frame: bool,
    pub has_psi: bool,   // This chunk starts with program specific information (PAT and PMT tables)
    pub has_vps: bool,   // Contains h265 VPS (always true for h264)
    pub has_sps: bool,   // Contains h264/h265 sequence parameter set
    pub has_pps: bool,   // Contains h264/h265 picture parameter set
    pub has_error: bool, // Had continuity counter errors or bad ts packets
    pub pts: Option<u64>, // presentation timestamp
    pub dts: Option<u64>, // decoding timestamp
    pub codec: Option<mpegts::Codec>,
    pub ts_data: Vec<u8>,// The mpegts data
    pub metadata: Vec<(u16, Option<u64>, Vec<u8>, bool)>,
}

impl TsChunkVideoFrame {
    pub fn build() -> TsChunkVideoFrame {
        TsChunkVideoFrame {
            is_iframe: false,
            is_nonref_frame: false,
            has_psi:   false,
            has_vps:   false,
            has_sps:   false,
            has_pps:   false,
            has_error: false,
            pts: None,
            dts: None,
            codec: None,
            ts_data: vec![],
            metadata: vec![],
        }
    }

    pub fn with_iframe(self, is_iframe: bool) -> TsChunkVideoFrame {
        TsChunkVideoFrame { is_iframe, ..self }
    }

    fn with_nonref(self, is_nonref_frame: bool) -> TsChunkVideoFrame {
        TsChunkVideoFrame { is_nonref_frame, ..self }
    }

    fn with_psi(self, psi: bool, vps: bool, sps: bool, pps: bool) -> TsChunkVideoFrame {
        TsChunkVideoFrame { has_psi: psi, has_vps: vps, has_sps: sps, has_pps: pps, ..self }
    }

    pub fn with_error(self, has_error: bool) -> TsChunkVideoFrame {
        TsChunkVideoFrame { has_error, ..self }
    }

    pub fn with_timestamps(self, pts: Option<u64>, dts: Option<u64>) -> TsChunkVideoFrame {
        TsChunkVideoFrame { pts, dts, ..self }
    }

    pub fn with_data(self, codec: Option<mpegts::Codec>, ts_data: Vec<u8>) -> TsChunkVideoFrame {
        TsChunkVideoFrame { codec, ts_data, ..self }
    }

    pub fn with_metadata(self, metadata: Vec<(u16, Option<u64>, Vec<u8>, bool)>) -> TsChunkVideoFrame {
        TsChunkVideoFrame { metadata, ..self }
    }
}

// Analyzes an incomming stream. Emits Vec<u8> containing all mpegts data only when whole video
// PES packet has been received.
// Strips away unwanted TS packets such as audio and data (optional)
// Removes all PSI from the stream and inserts it at the beginning of each output segment.
pub struct VideoStreamAnalyzer {
    // lists of all pids to be dropped from the stream before sending down a websocket
    pub pids_to_ignore: BTreeSet<u16>,
    pub parser: mpegts::Parser,
    video_pid: Option<(mpegts::Codec, u16)>,
    data: Vec<u8>,
    strip_audio: bool,
    strip_data: bool,
    bytes_since_last_iframe: usize,
    klv_pids: BTreeSet<u16>,
    klv_data: KlvCache,
}

impl VideoStreamAnalyzer {
    pub fn new(strip_audio: bool, strip_data: bool) -> VideoStreamAnalyzer {
        VideoStreamAnalyzer {
            pids_to_ignore: [0, 0x1FFF].iter().cloned().collect(),
            video_pid: Default::default(),
            data: vec![],
            parser: mpegts::Parser::new(),
            strip_audio,
            strip_data,
            bytes_since_last_iframe: 0usize,
            klv_pids: Default::default(),
            klv_data: Default::default(),
        }
    }

    pub fn bytes_since_last_iframe(&self) -> usize {
        self.bytes_since_last_iframe
    }

    pub fn bytes_since_last_frame(&self) -> usize {
        self.data.len()
    }

    pub fn get_video_pid(&self) -> Option<u16> {
        self.video_pid.map(|(_, pid)| pid)
    }

    fn get_data(&mut self) -> Vec<u8> {
        let mut vec = Vec::new();
        vec.extend_from_slice(&self.data);
        self.data.clear();
        vec
    }

    // Looks at TS data containing one PES packet worth of video data. Checks whether this PES
    // packet starts a new gop by analyzing the H264 bitstream.
    fn analyze_frame(&mut self, data: &[u8]) -> (bool, bool, bool, bool, bool, bool, Option<u64>, Option<u64>) {
        use mpegts::*;

        if self.video_pid.is_none() || data.len() < 188 {
            return (false, false, false, false, false, false, None, None);
        }

        let mut error = false;
        let mut video_cc = None;
        let mut pts = None;
        let mut dts = None;

        if let Some((codec, video_pid)) = self.video_pid.as_ref() {
            //try and find evidence that this video pes has an I-frame
            let mut parser: Box<dyn VideoNalParser> = match codec {
                Codec::H264 => Box::new(h264::H264NalParser::new()),
                Codec::H265 => Box::new(h265::H265NalParser::new()),
                Codec::AV1  => Box::new(av1::AV1OBUParser::new()),
                Codec::Klv | Codec::KlvMauc => unreachable!("klv pid is not video pid"),
                Codec::Other => unreachable!("other pid is not video pid"),
            };

            for ts_packet in data.chunks_exact(188) {
                match self.parser.parse(ts_packet) {
                    Ok(TsPacket{pid, cc, adaptation: _, payload: Some(Payload::Pes(PesHeader{ sid_type:SidType::Video, pts:ppts, dts:pdts, data, data_offset, .. }))}) if pid == *video_pid => {
                        check_cc_error(&mut error, cc, video_cc);
                        pts = ppts;
                        dts = pdts;
                        video_cc = Some(cc);
                        parser.push(&data[data_offset..]);
                    },
                    Ok(TsPacket{pid, cc, adaptation: _, payload: Some(Payload::Data(payload))}) if pid == *video_pid => {
                        check_cc_error(&mut error, cc, video_cc);
                        video_cc = Some(cc);
                        parser.push(payload);
                    },
                    Ok(_) => (),
                    Err(e) => {
                        error = true;
                        trace!("Error parsing ts_packet: {:?}", e);
                    }
                }
            }

            let NalAnalysisResult {
                i_frame,
                nonref_frame,
                found_vps,
                found_sps,
                found_pps
            } = parser.analyze();

            (i_frame, nonref_frame, found_vps, found_sps, found_pps, error, pts, dts)
        } else {
            (false, false, false, false, false, error, pts, dts)
        }

    }

    // Returns some data only on video PES boundary.  Returns buffer containing all TS packets
    // received within detected Video PES. Boolean flag is true when new gop starts with this PES.
    pub fn push_ts_packet(&mut self, ts_packet: &[u8]) -> Option<TsChunkVideoFrame> {
        use mpegts::*;

        match self.parser.parse(ts_packet) {
            Ok(TsPacket{ pid, cc:_, adaptation:_, payload: Some(Payload::Pes(pes))}) if pes.sid_type == SidType::Video => {
                // We have a new frame
                // self.video_pid = Some(pid);
                self.bytes_since_last_iframe += ts_packet.len();

                if let Some((_, video_pid)) = self.video_pid.as_ref() {
                    if pid != *video_pid {
                        return None;
                    }
                } else {
                    return None;
                }

                if let Some(pmt) = self.parser.pmt_pid {
                    self.pids_to_ignore.insert(pmt);
                }
                let vec = self.get_data();
                let vec = self.prepare_for_output(vec);
                self.data.extend_from_slice(ts_packet);
                let (gop_start, nonref_frame, vps, sps, pps, errors, pts, dts) = self.analyze_frame(&vec);

                if gop_start {
                    self.bytes_since_last_iframe = 0;
                }

                let has_psi = self.parser.last_pat.is_some() && self.parser.last_pmt.is_some();
                let codec = self.video_pid.as_ref().map(|(codec, _)| *codec);
                let klv = self.klv_data.consume();

                let chunk = TsChunkVideoFrame::build()
                    .with_iframe(gop_start)
                    .with_nonref(nonref_frame)
                    .with_psi(has_psi, vps, sps, pps)
                    .with_error(errors)
                    .with_timestamps(pts, dts)
                    .with_data(codec, vec)
                    .with_metadata(klv);

                return Some(chunk)
            },
            Ok(TsPacket{ pid, cc:_, adaptation: _ , payload: Some(Payload::Pes(pes))}) => {
                match pes.sid_type {
                    SidType::Audio if self.strip_audio => {
                        self.pids_to_ignore.insert(pid);
                    },
                    SidType::Other if self.strip_data => {
                        self.pids_to_ignore.insert(pid);
                    },
                    SidType::Klv => {
                        if self.strip_data {
                            self.pids_to_ignore.insert(pid);
                        } else {
                            self.klv_pids.insert(pid);

                            let mut data = &pes.data[pes.data_offset..];
                            let mut original_is_mauc = false;
                            if let Some(mpegts::Codec::KlvMauc) = self.parser.stream_types.get(&pid) {
                                // skip MAUC
                                if data.len() > 5 {
                                    data = &data[5..];
                                    original_is_mauc = true;
                                }
                            }

                            self.klv_data.start(pid, &data, pes.pts, pes.total_size, original_is_mauc);
                        }
                    }
                    _ => {
                    }
                }
            },
            Ok(TsPacket{ cc:_, adaptation: _ , payload: Some(Payload::Pmt(_pcr, prog_desc)), ..}) => {
                if let Some(ProgramDesc { stream_type, pid }) = prog_desc
                    .iter()
                    .find(|desc| desc.stream_type == Codec::H264 ||
                                 desc.stream_type == Codec::H265 ||
                                 desc.stream_type == Codec::AV1) {
                        self.video_pid = Some((*stream_type, *pid));
                }
            },
            Ok(TsPacket{ pid, cc:_, adaptation:_, payload: Some(Payload::Data(data)) }) if self.klv_pids.contains(&pid) => {
                // add data to klv pids
                if !self.strip_data {
                    self.klv_data.cont(pid, data);
                }
            },
            Ok(_) => (),
            Err(e) => trace!("Error parsing ts packet: {:?}", e),
        }

        self.data.extend_from_slice(ts_packet);
        self.bytes_since_last_iframe += ts_packet.len();
        None
    }

    fn prepare_for_output(&self, data: Vec<u8>) -> Vec<u8> {
        let mut v = Vec::with_capacity(data.len()+2*188);
        let parser = &self.parser;

        if let (Some(ref last_pmt), Some(ref last_pat)) = (parser.last_pmt, parser.last_pat) {
            v.extend_from_slice(last_pat);
            v.extend_from_slice(last_pmt);
        } else {
            warn!("last_pat or last_pmt not set");
            //TODO: run through all packets to try and find the PAT&PMT
        }

        for chunk in data.chunks_exact(188) {
            if self.pids_to_ignore.contains(&mpegts::read_pid(&chunk[1..3])) {
               //nop
            } else {
                v.extend_from_slice(chunk);
            }
        }

        v
    }
}

fn check_cc_error(error_indicator: &mut bool, cc: u8, prev_cc: Option<u8>) {
    if let Some(prev_cc) = prev_cc {
        if ((prev_cc + 1) % 16) != cc {
            *error_indicator = true;
            trace!("cc error: {} -> {}", prev_cc, cc);
        }
    }
}

#[derive(Default, Debug)]
pub(crate) struct KlvCache {
    open_buffers: HashMap<u16, (usize, Vec<u8>, bool)>,
    open_ts: HashMap<u16, u64>,
    collected_packets: Vec<(u16, Option<u64>, Vec<u8>, bool)>,
}

impl KlvCache {
    pub fn start(&mut self, pid: u16, data: &[u8], pts: Option<u64>, total_size: usize, original_is_mauc: bool) {
        if total_size <= data.len() {
            self.collected_packets.push((pid, pts, data.to_vec(), original_is_mauc));
        } else {
            if let Some((size, data, original_is_mauc)) = self.open_buffers.insert(pid, (total_size, data.to_vec(), original_is_mauc)) {
                let ts = self.open_ts.remove(&pid);
                if size != 0 && size < data.len() {
                    warn!("collecting rump klv packet, expected: {}B, got: {}B", size, data.len());
                }
                self.collected_packets.push((pid, ts, data, original_is_mauc));
            }
            if let Some(pts) = pts {
                let tsmap = self.open_ts.entry(pid).or_default();
                *tsmap = pts;
            }
        }
    }

    pub fn cont(&mut self, pid: u16, data: &[u8]) {
        let mut completed = false;

        if let Some((total_size, buf, original_is_mauc)) = self.open_buffers.get_mut(&pid) {
            buf.extend_from_slice(data);
            if *total_size <= buf.len() {
                completed = true;
            }
        }

        if completed {
            let removed_buffer = self.open_buffers.remove(&pid).unwrap();
            let data = removed_buffer.1;
            let original_is_mauc = removed_buffer.2;
            let ts = self.open_ts.remove(&pid);
            self.collected_packets.push((pid, ts, data, original_is_mauc));
        }
    }

    pub fn consume(&mut self) -> Vec<(u16, Option<u64>, Vec<u8>, bool)> {
        self.collected_packets.drain(..).collect()
    }
}

