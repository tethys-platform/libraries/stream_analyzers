use log::debug;
use parsers::mpegts;
use std::collections::HashMap;

// Receives mpegts packets and returns KLV data
pub struct KlvStreamAnalyzer {
    klv_streams: HashMap<u16, (usize, Vec<u8>)>,
    parser: mpegts::Parser,
}

impl KlvStreamAnalyzer {
    pub fn new() -> KlvStreamAnalyzer { 
        KlvStreamAnalyzer {
            klv_streams: Default::default(),
            parser: mpegts::Parser::new(),
        }
    }

    pub fn process_packet(&mut self, ts_packet: &[u8]) -> Option<Vec<u8>> {
        use mpegts::{Payload, SidType, TsPacket};
        let mut res = vec![];
        let mut current_pid = 0;

        match self.parser.parse(ts_packet) {
            Ok(TsPacket{ pid, cc:_, adaptation:_, payload: Some(Payload::Pes(pes))}) if pes.sid_type == SidType::Klv => {
                current_pid = pid;
                // Check if we had previous pes buffered data
                if let Some((len, data)) = self.klv_streams.remove(&pid) {
                    debug!("KLV pes, pid: {:?}, sending out: {} of {}", pid, data.len(), len);
                    res.extend_from_slice(&data);
                }

                let mut data = &pes.data[pes.data_offset..];
                if let Some(mpegts::Codec::KlvMauc) = self.parser.stream_types.get(&pid) {
                    if data.len() > 5 {
                        data = &data[5..];
                    }
                }

                self.klv_streams.insert(pid, (pes.total_size, data.to_vec()));
            },
            Ok(TsPacket{ pid, cc:_, adaptation:_, payload: Some(Payload::Data(data)) }) => {
                current_pid = pid;
                self.klv_streams.entry(pid)
                    .and_modify(|entry| {
                        entry.1.extend_from_slice(data);
                    });
                },
            _ => (),
        }

        if self.klv_streams
            .get(&current_pid)
                .map(|(size, vec)| {
                    *size != 0 && vec.len() >= *size
                })
        .unwrap_or(false) {

            if let Some((_len, data)) = self.klv_streams.remove(&current_pid) {
                res.extend_from_slice(&data);
            }
        }

        if res.is_empty() {
            None
        } else {
            Some(res)
        }
    }
}
