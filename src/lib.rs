pub mod video_stream_analyzer;
pub mod klv_stream_analyzer;

#[cfg(test)]
mod tests {
    use hex_literal::*;
    const TEST_DATA: [u8; 3196] = hex!("
        474000140000b00d0001c100000001f0002ab104b2ffffffffffffffffff
        ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
        ffffffffffffffff475000140002b01d0001c10000e101f00006e100f006
        05044b4c56411be101f000d0eac01affffffffffffffffffffffffffffff
        ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffffffffffff474101380710000332127e000000
        01e0000080c00a31001de36511001db4790000000109f000000001419e42
        788efffb3fa04743ead574a5c84dfcbcf40b28f90f81f51be5875ef20f8d
        364f9db1123fbfc3f17446c30286913571fe7c0748cf0b7a2afdb9534d98
        972b61f1d574aff2f7c3fb526f2d5f9abdbc2cdfd4fa316257cb485c6f5b
        a2aafb4f89d30b36ef2c736c5934375ce6cc87133f4b86f9c83b2681f2fd
        4d02817b4960ebc99d0d26e59227493fa7b6ee25f188ba8347010119d215
        86e41075ccb1469fe434ddb3d8e19facf90975173a30aaa779abe30879f1
        9c67408189d98d9906320e958d418ef918dd88a023679f513983f9b5176a
        a06a8fbdb7a0724fcc08ced0deba40851a5e2b52c308acd51737c0df7b52
        8a01a709b38ae7b479773c59869e5a8cc938a009dd8ca3a8bc4f8243f9f1
        1bb192c4975947938f3311aa2006d95e50a4a5ffa3b797db713047a760cb
        204419e5795e16caa73e761cc257160373e3f807b661070b213f6f2e6693
        1ca54701011a1352d8ef924ab3d63ac55cd3c156314aa23edda6598a7c22
        2eb1f69e0220683111fb3b40267b7656f530455fd2bf6405c557f1d6747b
        fb40b0e1285dfae6683f41a0905dabceddc055a3da2d068d45318f6ae071
        0b4090bcd55391932a99ce9941dd4d248dcfd272b40b1559f1944ad84413
        90fbfb72536a82bb6eaf1d3c677638e9b71dfb9934cc1b84f6effb245b38
        70ff5e2a5d11254f3ff37af49fa63cdb5ae07a6382f47087052e4e4bbb3f
        6d0a9df59565ca94c2894701013b0400ffffffe8273e551d1167324e4eb1
        a757175be9e0778e90b6a644f7eb34d957dab8a19f6f54f1be97e3b16a24
        2b870eb4b0a8f1d451bb24e45ebd4d8e761ccbfce16454759d82f818e222
        487d5c4c20c6fcc993637794624b0646a32b375b9f57aafce40f18930c57
        01cb87827538c710e2300d5de62b877c1fac1f3f56ddf92c47051fb4b643
        a604adb5722c979e39c0b96021f4f9c9e4bb82b6a335285c97e93c613e4d
        6ee639fdd8ff4992900b9e2af3bd90702781474100360140000001fc010b
        84800521001dc03b060e2b34020b01010e0103010100000081f102080004
        ca14211958890315455352495f4d657461646174615f436f6c6c65637404
        064e39373832360502e01906023a330702e8670a0543323038420b000c00
        0d043a7ef6240e04b5738ffc0f0231b6100203f4110202391204bf16c16d
        1304f23333331404000000001504001e36951602000017043a7a8dbb1804
        b570460a1902239d1a02fea71b02ffbf1c02fff91d02fe331e0247010037
        5c00ffffffffffffffffffffffffffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
        ffffff01491f02003f20020008210201b72f0100302a0101010201010304
        2f2f43410400050006024341151000000000000000000000000000000000
        160200053801003b08466972656269726441010148080000000000000000
        0102a1b2474000140000b00d0001c100000001f0002ab104b2ffffffffff
        ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffff475000140002b01d0001c10000e101f00006
        e100f00605044b4c56411be101f000d0eac01affffffffffffffffffffff
        ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffffffffffffffffffff4741011c000001e00000
        80800521001dcbef0000000109f000000001019e6174421ffefb359b8d7c
        63b067f80cccac077167b093462fb4b690eb2bfe77e9acbb47652cdfcf28
        e59028e3174527e18c5b3dd2c1a7d60b0a4ee0e1575759b21320f7d6a7c1
        f90fe4c5949190cd113bbe75f776d2839eab1281453452a83c8240119cf7
        8ec9ed10f2611ff5fe3f741d213afc2928dda915c4504023566b074c0df6
        ffebe1bfbc85806ab480560c00452fb5e72894fd113f756a8170cc3e4701
        013d2700ffffffffffffffffffffffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffffd8509e2b76b74ff4ed53a8099de9ba80569c
        c43afef04fc29399a652f83e2f36635e734486b8886918926e9fc1536502
        c25a57455a4811492402683c9dc4a4f335fdb9dbd91a5c3486534c0e60b1
        f20b528d4d0266865669105c86a1c1f046aaf993e8ad597e359e04c9cafb
        9ea9cbde06b944a89ba9533eb481c4b95c9f085b299ac40b422e4a293b80
        ad93ec05d4e0474000140000b00d0001c100000001f0002ab104b2ffffff
        ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffffffff475000140002b01d0001c10000e101f0
        0006e100f00605044b4c56411be101f000d0eac01affffffffffffffffff
        ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffffffffffffffffffffffff4741013e07100003
        3dcd7e00000001e0000080c00a31001dfadb11001de3650000000109f000
        000001019e636a421ffc044017f9573d4fcb48f0dc1a6676e847a4d321c9
        bd55966a7036f83cc0ecbf38c726298ef0c82fdac27d195c84c868100538
        d0b6d67a048c5a374de78d2011c18effefdc4fcdd2cd50a2241fe8b2b473
        5252a6a0d08b1b3b79e8ac90d1d429e3c10894eb1e8040efbe3db4caae35
        b0ff1faf6dd9d5bd7a8cc99a2a484688da031e41e60222543bdab2496550
        4701011f6229900f1cd2923ff963f1b4cc0f283cc2936765fa7f7161f8dc
        aabf375ddfeab16439179f7962f50887fa76168f85bce07a23726406e163
        53770e79427cb2654e6ffb31a17c36ec8009009e29177842a864af65f54c
        c5cafbad967a02ac896591bb584d3fee5648dc14d3790211aaa19b2475bb
        14b6838a4cbbb3f8b3bd7a6d613b5d64300d89d799144034074ade97c099
        70e1545db4209882eaccd0c201b0b7ca1dff486bf5908b8db3aafe2e2a22
        fee107c6b4fce53d470101307200ffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
        ffffffffffffff81becc65aaadacaebf391b0feafc30372500d1ff7cb527
        9e20393ebe8d5ca74ac3b79d9615030d31e7e7dcd26f292c207f45369b4f
        39de072031ca87bc09e8d664228e7181");

    #[test]
    fn test_klv_analyzer() {
        let mut analyzer = super::klv_stream_analyzer::KlvStreamAnalyzer::new();
        let mut res = vec![];

        for chunk in TEST_DATA.chunks_exact(188) {
            match analyzer.process_packet(&chunk) {
                Some(data) => res.push(data),
                _ => (),
            }
        }

        assert_eq!(res.len(), 1);
    }

    #[test]
    fn test_video_analyzer() {
        let mut analyzer = super::video_stream_analyzer::VideoStreamAnalyzer::new(false, false);
        let mut res = vec![];

        for chunk in TEST_DATA.chunks_exact(188) {
            match analyzer.push_ts_packet(&chunk) {
                Some(data) => res.push(data),
                _ => (),
            }
        }

        assert_eq!(res.len(), 3);
    }

    #[test]
    fn klvcache_single_packet() {
        let mut cache = super::video_stream_analyzer::KlvCache::default();
        cache.start(10, &[0; 160], None, 160, false);
        assert_eq!(cache.consume().len(), 1);
    }

    #[test]
    fn klvcache_multiple_packets() {
        let packets: Vec<Vec<u8>> = vec![vec![0; 160], vec![0; 160], vec![0; 50]];
        let mut cache = super::video_stream_analyzer::KlvCache::default();

        cache.start(10, &packets[0], None, 320, false);
        cache.cont(10, &packets[1]);
        cache.cont(10, &packets[2]);

        assert_eq!(cache.consume().len(), 1);
    }
}

